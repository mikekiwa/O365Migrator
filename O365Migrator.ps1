﻿#Module name: O365Migrator
#Author: Jos Lieben
#Date: 16-05-2017
#Script help: www.lieben.nu
#Purpose: provision mysites and set admin permissions automatically, clean up homedirectories and upload in bulk
#         upload fileshare in its entirety to sharepoint online
#Requirements:
#Sharepoint Client components: https://www.microsoft.com/en-us/download/details.aspx?id=42038
#Sharepoint Online components: http://www.microsoft.com/en-us/download/details.aspx?id=35588
#Office 365 online module: https://msdn.microsoft.com/en-us/library/dn975125.aspx (only Step 1 is required)
#Powershell 4
#.NET 4.5
#run “Set-Executionpolicy Unrestricted” in an elevated powershell window
#Windows 7+ or Windows Server 2008+
######## 
#TODO:
#Add Onedriver user as owner instead of admin
#Improve differential sync (deletes)
#Scheduled task sync
#Add cancel/abort button
#add IRM to Onedrive for Business
#Changelog: 
######## 
#V0.6: added basic folder upload support to Sharepoint (ported and improved functionality from O365Uploader)
#V0.7: added data done, time taken, time estimated and improved display
#V0.8: added multithreading (upload per subfolder of the root path) to folderUpload
#V0.9: new detection method for document library in Onedrive (due to a reported bug in localization mismatch between admin/user)
#V0.9: added BASIC differential sync: existing files will not be overwritten if the last modified date is the same
#V0.9: added subfolder targeting
#V0.91: folder ignore list parameterized in top level script
#V0.92: retry file added to retry failed uploads automatically
#V0.93: fixed a bug in passing the FolderIgnoreList as array vs string in checking and match vs contain in correcting
#V0.94: fixed a bug in array translation and erronous logging of failed files that didn't fail
#V0.95: Properly set created and modified dates for libraries, auto disable and re-enable force checkout and versioning
#V0.96: v1.10 of cleaner (less blocked characters and different folder checks), Scan Only mode for both upload scenario's
#V0.97: v1.12 of cleaner, includes AlphaFS and additional improvements in cleanup and defaults
#V0.98: v1.13 of cleaner, changed .net dependency with a fix to prevent errors for a new() override
#V0.99: when a homedir upload fails, versioning could remain disabled in certain scenario's, fixed that
#V0.99: retry functionality removed (too buggy) and now show author on homedir folders+files if the author can be found (uses the username column in the csv file)
#V0.99: unAuthorize button added, to remove permissions for the site admin
#V1.00: check if folder upload exceeds 5000 items
#V1.00: allow uploading folders without running verify first
#V1.00: chunked file uploads (thanks @Allan Wallace!)
#V1.00: retry uploads up to 3 times
#V1.00: update to replace _w or _t in folder names only if that IS the foldername, vs if the name starts with _w or _t

$scriptName = "O365Migrator"
$scriptVersion = "1.00"
$verifyLogPath = $env:APPDATA+"\O365MigratorDataCleanerResults.log"
$folderIgnoreList = "Non-Migrateable,Application Data,My Oce Printer Driver Templates,Windows,Remote Assistance Logs" #top level folders to ignore, seperate with a comma
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$CSVdelimiter = $Null           #CSV column delimiter, uses your local settings if not configured

#Check if elevated
If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")){   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}

Write-Host "Welcome to $scriptName v$scriptVersion...." -ForegroundColor Green
Write-Host "Loading components and starting UI, please wait...." -ForegroundColor Green
Write-Host ""
Write-Host ""
Write-Host ""
Write-Host ""
Write-Host ""

$debug = $False
$maxThreads  = 10
$Jobs = @()
$SleepTimer = 50
$ISS = [system.management.automation.runspaces.initialsessionstate]::CreateDefault()
$RunspacePool = [runspacefactory]::CreateRunspacePool(1, $maxThreads, $ISS, $Host)
$RunspacePool.ApartmentState = "STA"
$RunspacePool.Open()
$communicatieObject = [hashtable]::Synchronized(@{})
$communicatieObject.main = @{}
$communicatieObject.main.description = "datacommunicatie tussen threads in de runspace"
$communicatieObject.status = @{}
$communicatieObject.status.o365connected = 0
$communicatieObject.status.o365tenant = ""
$communicatieObject.status.hdCSVverified = 0
$communicatieObject.status.hdVerifying = 0
$communicatieObject.status.hdVerifying_done = 0
$communicatieObject.status.hdVerifying_failed = 0
$communicatieObject.status.folderVerifying = 0
$communicatieObject.status.folderVerifyingItemCount = 0
$communicatieObject.status.folderVerifying_done = 0
$communicatieObject.status.folderVerifying_failed = 0
$communicatieObject.status.folderVerifying_totalSize = 0
$communicatieObject.status.Uploading_ByteCount = 0
$communicatieObject.status.Uploading_failedObjectCount = 0
$communicatieObject.status.Uploading_failedObjects = @{}
$communicatieObject.status.verifying_totalSize = 0
$communicatieObject.status.hdUploading = 0
$communicatieObject.status.hdUploading_Starttime = 0
$communicatieObject.status.hdUploading_done = 0
$communicatieObject.status.hdUploading_failed = 0
$communicatieObject.status.folderUploading = 0
$communicatieObject.status.folderUploading_Starttime = 0
$communicatieObject.status.folderUploading_done = 0
$communicatieObject.status.folderUploading_failed = 0
$communicatieObject.status.hdCSVimported = 0
$communicatieObject.hdCSV = @{} #will hold CSV data
$communicatieObject.functionCalls = @{}
$communicatieObject.jobList = @()
$communicatieObject.jobParameters = @{}
$communicatieObject.jobList += "gui.ps1"
$communicatieObject.jobParameters.0 = @{"communicatieObject" = $communicatieObject}
$script:jobIndex = 0
$script:finishedJobs = 0
$script:runningJobs = 0
$script:waitingJobs = @($communicatieObject.jobList).Count
$script:totalJobs = @($communicatieObject.jobList).Count

#Set delimiter based on user localized settings if not configured
if($CSVdelimiter -eq $Null) {
    $CSVdelimiter = (Get-Culture).TextInfo.ListSeparator
}

function Pause{
   Read-Host 'Press Enter to continue...' | Out-Null
}

function endScript{
   Read-Host 'Script has finished, press Enter to exit...' | Out-Null
   Exit
}

function JosL-WebRequest{
    Param(
        $url
    )
    $maxAttempts = 3
    $attempts=0
    while($true){
        $attempts++
        try{
            $retVal = @{}
            $request = [System.Net.WebRequest]::Create($url)
            $request.TimeOut = 5000
            $request.UserAgent = "Lieben Consultancy"
            $response = $request.GetResponse()
            $retVal.StatusCode = $response.StatusCode
            $retVal.StatusDescription = $response.StatusDescription
            $retVal.Headers = $response.Headers
            $stream = $response.GetResponseStream()
            $streamReader = [System.IO.StreamReader]($stream)
            $retVal.Content = $streamReader.ReadToEnd()
            $streamReader.Close()
            $response.Close()
            return $retVal
        }catch{
            if($attempts -ge $maxAttempts){Throw}else{sleep -s 2}
        }
    }
}

function versionCheck{
    Param(
        $currentVersion
    )
    $apiURL = "http://www.lieben.nu/lieben_api.php?script=O365Migrator&version=$currentVersion"
    $apiKeyword = "latestO365MigratorVersion"
    try{
        $result = JosL-WebRequest -Url $apiURL
    }catch{
        Throw "Failed to connect to API url for version check: $apiURL $($Error[0])"
    }
    try{
        $keywordIndex = $result.Content.IndexOf($apiKeyword)
        if($keywordIndex -lt 1){
            Throw ""
        }
    }catch{
        Throw "Connected to API url for version check, but invalid API response"
    }
    $latestVersion = $result.Content.SubString($keywordIndex+$apiKeyword.Length+1,4)
    if($latestVersion -ne $currentVersion){
        Throw "O365Migrator version mismatch, current version: v$currentVersion, latest version: v$latestVersion"
    }
}

function runProcess ($cmd, $params, $windowStyle=1) {
    $p = new-object System.Diagnostics.Process
    $p.StartInfo = new-object System.Diagnostics.ProcessStartInfo
    $exitcode = $false	
    $p.StartInfo.FileName = $cmd
    $p.StartInfo.Arguments = $params
    $p.StartInfo.UseShellExecute = $False
    $p.StartInfo.RedirectStandardError = $True
    $p.StartInfo.RedirectStandardOutput = $True
    $p.StartInfo.WindowStyle = $windowStyle; #1 = hidden, 2 =maximized, 3=minimized, 4=normal
    $null = $p.Start()
    $output = $p.StandardOutput.ReadToEnd()
    $exitcode = $p.ExitCode
    $p.Dispose()	
    $exitcode
    $output
}

try{
    $alphaFSPath = Join-Path $scriptPath -ChildPath "AlphaFS.dll"
    Add-Type -Path $alphaFSPath -ErrorAction Stop
    Write-Host "AlphaFS .NET library loaded properly" -ForegroundColor Green
}catch{
    write-host "Failed to load AlphaFS .NET library, make sure you unblock this file in explorer: $($Error[0])" -ForegroundColor Red
    endScript
}

#Load WPF Framework (.NET)
try{
    Add-Type -AssemblyName PresentationCore,PresentationFramework,WindowsBase,system.windows.forms
    Write-Host "WPF assemblies: OK" -ForegroundColor Green
} catch {
    $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Failed to load Windows Presentation Framework assemblies, install .NET 4 or higher", "$scriptName v$scriptVersion" , 0)
    Throw "Failed to load Windows Presentation Framework assemblies, install .NET 4 or higher"
    endScript
}

#Load sharepoint libraries
try{
    [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client") | Out-Null    [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client.Runtime") | Out-Null    [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client.UserProfiles") | Out-Null
    Write-Host "Sharepoint Client Libraries: OK" -ForegroundColor Green
} catch {
    $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Please install the Sharepoint 2013 Client Components from https://www.microsoft.com/en-us/download/details.aspx?id=42038", "$scriptName v$scriptVersion" , 0)
    Throw "Failed to load Sharepoint Extensions, please install the Sharepoint 2013 Client Components from https://www.microsoft.com/en-us/download/details.aspx?id=42038"
    endScript
}

#load azure/o365 modules
$env:PSModulePath += ";C:\Windows\System32\WindowsPowerShell\v1.0\Modules\"
try{
    Import-Module msonline -ErrorAction Stop -DisableNameChecking | Out-Null
    Write-Host "AzureAD Module: OK" -ForegroundColor Green
}catch{
    $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Failed to load AzureAD module, please follow Step 1 from https://technet.microsoft.com/library/dn975125.aspx", "$scriptName v$scriptVersion" , 0)
    Throw "Failed to load AzureAD module, please follow Step 1 from https://technet.microsoft.com/library/dn975125.aspx"
    endScript
}

#load Sharepoint Online module
try{
    Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking -ErrorAction Stop | Out-Null
    Write-Host "Sharepoint Online Module: OK" -ForegroundColor Green
}catch{
    $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Failed to load Sharepoint Online module, please install http://www.microsoft.com/en-us/download/details.aspx?id=35588", "$scriptName v$scriptVersion" , 0)
    Throw "Failed to load Sharepoint Online module, please install http://www.microsoft.com/en-us/download/details.aspx?id=35588"
    endScript
}

#versionCheck
try{
    versionCheck -currentVersion $scriptVersion
    write-host "Version check: OK" -ForegroundColor Green
}catch{
    Write-Host "Version check: FAILED: $($Error[0])" -ForegroundColor "Red"
}

Write-Host "Opening UI now..." -ForegroundColor Green

#returns rounded number + suffix
function returnSizeString{
    param(
        [Decimal]$bytes
    )
    $suffix = "Bytes"
    if($bytes -gt 1024){
        $bytes = $bytes / 1024
        $suffix = "Kb"
    }
    if($bytes -gt 1024){
        $bytes = $bytes / 1024
        $suffix = "Mb"
    }
    if($bytes -gt 1024){
        $bytes = $bytes / 1024
        $suffix = "Gb"
    }
    if($bytes -gt 1024){
        $bytes = $bytes / 1024
        $suffix = "Tb"
    }
    $bytes = [System.Math]::Round($bytes, 2)
    $bytes
    $suffix
}

function startThread(){
    Param(
        [String]$scriptName,
        [Hashtable]$communicatieObject,
        [Hashtable]$parameters
    )
    $secondaryScriptPath = Join-Path -Path $scriptPath -ChildPath $scriptName
    $OFS = "`r`n"
    $Code = [ScriptBlock]::Create($(Get-Content $secondaryScriptPath))
    if($Code -eq $Null){
        Throw "File does not exist or is empty at $secondaryScriptPath"
    }
    Remove-Variable OFS
    $thread = [PowerShell]::Create().AddScript($Code)
    if($parameters){
        foreach($Key in $parameters.Keys){
            $thread.AddParameter($Key, $parameters.$Key)
        }
    }
    $thread.RunspacePool = $RunspacePool
    $Handle = $thread.BeginInvoke()
    $Job = "" | Select-Object Handle, Thread, object
    $Job.Handle = $Handle
    $Job.Thread = $thread
    $Job.Object = $secondaryScriptPath
    Write-Host "$(Get-Date): Starting new thread: $($Job.Object)"
    return $Job
}


function pushToUILog{
    Param(
        [String]$logLine,
        [Switch]$append
    )
    if(-NOT $append){
        $logLine = "`n$(Get-Date): $logLine"
    }
    $communicatieObject.Interface.iLog.Dispatcher.invoke("Normal",[action]{$communicatieObject.Interface.iLog.AppendText($logLine)})
    $communicatieObject.Interface.iLog.Dispatcher.invoke("Normal",[action]{$communicatieObject.Interface.iLog.ScrollToEnd()})
    Write-Host $logLine
}

Function readFromUI{
    Param(
        [String]$controlName,
        [String]$propertyName
    )
    $communicatieObject.Interface.$controlName.Dispatcher.invoke("Normal",[action]{$communicatieObject.Interface.varReader = $communicatieObject.Interface.$controlName.$propertyName}) | Out-Null
    Return $communicatieObject.Interface.varReader
}

Function setUI{
    Param(
        [String]$controlName,
        [String]$propertyName,
        [String]$newValue
    )
    $communicatieObject.Interface.$controlName.Dispatcher.invoke("Normal",[action]{$communicatieObject.Interface.$controlName.$propertyName = $newValue})
}

function connectToOffice365(){
    $communicatieObject.functionCalls.connectToOffice365 = 0

    $login = readFromUI -controlName iLoginBox -propertyName Text
    $password = readFromUI -controlName iPasswordBox -propertyName Password
    pushToUILog -logLine "Attempting to connect to Office 365 as $login..." 

    #build Credential Object
    $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force
    $Credentials = New-Object System.Management.Automation.PSCredential ($login, $secpasswd)
    #connect to MSOL
    try{
        Connect-MsolService -ErrorAction Stop -Credential $Credentials
        $tenant = @(get-msoldomain | where-object {$_.Name -Match ".onmicrosoft.com" -and $_.Name -NotMatch "mail.onmicrosoft.com"})[0].Name
        $tenant = $tenant.SubString(0,$tenant.IndexOf("."))
        $communicatieObject.status.o365tenant = $tenant
        setUI -controlName "iConnectResult" -propertyName "Content" -newValue "Connected to $tenant"
        setUI -controlName "iConnectResult" -propertyName "Foreground" -newValue "Green"
        pushToUILog -logLine "Connected to Office 365, tenant: $tenant"
        $communicatieObject.status.o365connected = 1
    }catch{
        setUI -controlName "iConnectResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iConnectResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed to connect to Office 365: $($Error[0])"  
        $communicatieObject.status.o365connected = 0      
    }
}

function provisionO4B(){
    $communicatieObject.functionCalls.provisionO4B = 0
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    #fetch all UPN's
    try{
        $users = Get-MsolUser -All | Select-Object UserPrincipalName
    }catch{
        setUI -controlName "iProvisionResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iProvisionResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed to retrieve user list from Office 365: $($Error[0])"
        return
    }

    #Build sP object
    $spURL = "https://$($communicatieObject.status.o365tenant)-admin.sharepoint.com"
    pushToUILog -logLine "Attempting to connect to $spURL..."
    $client = New-Object Microsoft.SharePoint.Client.ClientContext($spURL)    $clientweb = $client.Web    $login = readFromUI -controlName iLoginBox -propertyName Text
    $password = readFromUI -controlName iPasswordBox -propertyName Password
    #build Credential Object
    $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force    $client.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($login,$secpasswd)    #Connect    $client.Load($clientweb)    try{        $client.ExecuteQuery()        pushToUILog -logLine "Connected to Sharepoint Online"    }catch{        setUI -controlName "iProvisionResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iProvisionResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Unable to connect to Sharepoint Online: $($Error[0])"        return    }    #get loader    try{        $loader =[Microsoft.SharePoint.Client.UserProfiles.ProfileLoader]::GetProfileLoader($client)        $profile = $loader.GetUserProfile()        $client.Load($profile)        $client.ExecuteQuery()
    }catch{
        setUI -controlName "iProvisionResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iProvisionResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed when executing remote query: $($Error[0])"
        return
    }

    #enqueue per 10    $total = $users.Count    $failures = $False    $batchjob = @()    setUI -controlName "iBar" -propertyName "value" -newValue 0    pushToUILog -logLine "Adding $total users to queue..."    for($i = 0; $i -lt $total; $i++){        try{$percentComplete = ($i / $total) * 100}catch{$percentComplete = 0}        $batchjob += $users[$i].UserPrincipalName        if($i+1 -eq $total -or $batchjob.Count -gt 10){            #enqueue in loader            try{                $loader.CreatePersonalSiteEnqueueBulk($batchjob)                 $loader.Context.ExecuteQuery()            }catch{                $failures = $True                setUI -controlName "iProvisionResult" -propertyName "Content" -newValue "Failed"
                setUI -controlName "iProvisionResult" -propertyName "Foreground" -newValue "Red"
                pushToUILog -logLine "Error in batch job $batchjob"                pushToUILog -logLine "Failed when executing: $($Error[0])"            }            $batchjob = @()        }        setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete    }
    setUI -controlName "iBar" -propertyName "value" -newValue 100
    if($failures){
        setUI -controlName "iProvisionResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iProvisionResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Completed provisioning, but there were some errors"
    }else{
        setUI -controlName "iProvisionResult" -propertyName "Content" -newValue "Completed"
        setUI -controlName "iProvisionResult" -propertyName "Foreground" -newValue "Green"
        pushToUILog -logLine "Completed provisioning $total users with no errors. If you intend to set permissions, wait a few minutes before proceeding!"
    }
    return
}

function authorizeO4B(){
    $communicatieObject.functionCalls.authorizeO4B = 0
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    #Build sP credential object    $login = readFromUI -controlName iLoginBox -propertyName Text
    $password = readFromUI -controlName iPasswordBox -propertyName Password
    #build Credential Object
    $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force    $creds = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($login,$secpasswd)    $Credentials = New-Object System.Management.Automation.PSCredential ($login, $secpasswd)    #build proxy    try{        $spURL = "https://$($communicatieObject.status.o365tenant)-admin.sharepoint.com"        $spMyURL = "https://$($communicatieObject.status.o365tenant)-my.sharepoint.com"        pushToUILog -logLine "Attempting to connect to $spURL..."        $proxyaddr = "$spURL/_vti_bin/UserProfileService.asmx?wsdl"        $UserProfileService= New-WebServiceProxy -Uri $proxyaddr -UseDefaultCredential False        $UserProfileService.Credentials = $creds        $strAuthCookie = $creds.GetAuthenticationCookie($spURL)        $uri = New-Object System.Uri($spURL)        $container = New-Object System.Net.CookieContainer        $container.SetCookies($uri, $strAuthCookie)        $UserProfileService.CookieContainer = $container
        pushToUILog -logLine "Connected to Sharepoint Online"
    }catch{
        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed when trying to connect: $($Error[0])"
        return
    }
    try{        $UserProfileResult = $UserProfileService.GetUserProfileByIndex(-1)        $NumProfiles = $UserProfileService.GetUserProfileCount()        $i = 1        $ProfileURLs = @()        pushToUILog -logLine "Found $NumProfiles profiles in Onedrive for Business (this may be more than your number of users)"    }catch{        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed when trying to retrieve profiles: $($Error[0])"
        return    }
    pushToUILog -logLine "Inspecting profiles now to check if they have been provisioned..."
    try{        While ($UserProfileResult.NextValue -ne -1){            try{$percentComplete = ($i / $NumProfiles) * 100}catch{$percentComplete = 0}            setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete            $Prop = $UserProfileResult.UserProfile | Where-Object { $_.Name -eq "PersonalSpace" }             $Url= $Prop.Values[0].Value            if ($Url) {                $ProfileURLs += $Url            }            $UserProfileResult = $UserProfileService.GetUserProfileByIndex($UserProfileResult.NextValue)            $i++
        }
    }catch{
        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed while trying to inspect profiles: $($Error[0])"
        return        
    }
    pushToUILog -logLine "Inspection complete, $($ProfileURLs.Count) provisioned profiles found, will now reconnect to Sharepoint"
    setUI -controlName "iBar" -propertyName "value" -newValue 100
    try{        Connect-SPOService -Url $spURL -Credential $Credentials        pushToUILog -logLine "Reconnected, processing profiles now."
        setUI -controlName "iBar" -propertyName "value" -newValue 0    }catch{        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Could not connect to Sharepoint Online: $($Error[0])"
        return      }

    $total = $ProfileURLs.Count
    $done = 0
    $failures = $False
    foreach($profileURL in $ProfileURLs){        $done++        try{$percentComplete = ($done / $total) * 100}catch{$percentComplete = 0}        setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete        $fullPath = "$spMyURL$profileURL".TrimEnd("/")        try{            Set-SPOUser -ErrorAction Stop -Site $fullPath -LoginName $login -IsSiteCollectionAdmin $true        }catch{
            $failures = $True
            pushToUILog -logLine "Failed to add permissions to $($fullPath): $($Error[0])"            }    }

    setUI -controlName "iBar" -propertyName "value" -newValue 100
    if($failures){
        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Completed setting permissions, but there were some errors. Provisioning was likely not yet completed, try again in a few minutes."
    }else{
        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Completed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Green"
        pushToUILog -logLine "Completed setting permissions on $total users with no errors"
    }
    return
}

function unAuthorizeO4B(){
    $communicatieObject.functionCalls.unAuthorizeO4B = 0
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    #Build sP credential object    $login = readFromUI -controlName iLoginBox -propertyName Text
    $password = readFromUI -controlName iPasswordBox -propertyName Password
    #build Credential Object
    $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force    $creds = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($login,$secpasswd)    $Credentials = New-Object System.Management.Automation.PSCredential ($login, $secpasswd)    #build proxy    try{        $spURL = "https://$($communicatieObject.status.o365tenant)-admin.sharepoint.com"        $spMyURL = "https://$($communicatieObject.status.o365tenant)-my.sharepoint.com"        pushToUILog -logLine "Attempting to connect to $spURL..."        $proxyaddr = "$spURL/_vti_bin/UserProfileService.asmx?wsdl"        $UserProfileService= New-WebServiceProxy -Uri $proxyaddr -UseDefaultCredential False        $UserProfileService.Credentials = $creds        $strAuthCookie = $creds.GetAuthenticationCookie($spURL)        $uri = New-Object System.Uri($spURL)        $container = New-Object System.Net.CookieContainer        $container.SetCookies($uri, $strAuthCookie)        $UserProfileService.CookieContainer = $container
        pushToUILog -logLine "Connected to Sharepoint Online"
    }catch{
        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed when trying to connect: $($Error[0])"
        return
    }
    try{        $UserProfileResult = $UserProfileService.GetUserProfileByIndex(-1)        $NumProfiles = $UserProfileService.GetUserProfileCount()        $i = 1        $ProfileURLs = @()        pushToUILog -logLine "Found $NumProfiles profiles in Onedrive for Business (this may be more than your number of users)"    }catch{        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed when trying to retrieve profiles: $($Error[0])"
        return    }
    pushToUILog -logLine "Inspecting profiles now to check if they have been provisioned..."
    try{        While ($UserProfileResult.NextValue -ne -1){            try{$percentComplete = ($i / $NumProfiles) * 100}catch{$percentComplete = 0}            setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete            $Prop = $UserProfileResult.UserProfile | Where-Object { $_.Name -eq "PersonalSpace" }             $Url= $Prop.Values[0].Value            if ($Url) {                $ProfileURLs += $Url            }            $UserProfileResult = $UserProfileService.GetUserProfileByIndex($UserProfileResult.NextValue)            $i++
        }
    }catch{
        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Failed while trying to inspect profiles: $($Error[0])"
        return        
    }
    pushToUILog -logLine "Inspection complete, $($ProfileURLs.Count) provisioned profiles found, will now reconnect to Sharepoint"
    setUI -controlName "iBar" -propertyName "value" -newValue 100
    try{        Connect-SPOService -Url $spURL -Credential $Credentials        pushToUILog -logLine "Reconnected, processing profiles now."
        setUI -controlName "iBar" -propertyName "value" -newValue 0    }catch{        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Could not connect to Sharepoint Online: $($Error[0])"
        return      }

    $total = $ProfileURLs.Count
    $done = 0
    $failures = $False
    foreach($profileURL in $ProfileURLs){        $done++        try{$percentComplete = ($done / $total) * 100}catch{$percentComplete = 0}        setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete        $fullPath = "$spMyURL$profileURL".TrimEnd("/")        if($profileURL.Contains($login.Replace("@","_").Replace(".","_"))){            pushToUILog -logLine "skipping admin's own profile"            continue        }        try{            Set-SPOUser -ErrorAction Stop -Site $fullPath -LoginName $login -IsSiteCollectionAdmin $false        }catch{
            $failures = $True
            pushToUILog -logLine "Failed to remove permissions from $($fullPath): $($Error[0])"            }    }

    setUI -controlName "iBar" -propertyName "value" -newValue 100
    if($failures){
        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Failed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Red"
        pushToUILog -logLine "Completed removing permissions, but there were some errors. Provisioning was likely not yet completed, try again in a few minutes."
    }else{
        setUI -controlName "iAuthorizeResult" -propertyName "Content" -newValue "Completed"
        setUI -controlName "iAuthorizeResult" -propertyName "Foreground" -newValue "Green"
        pushToUILog -logLine "Completed removing permissions on $total users with no errors"
    }
    return
}

function hdCSVCheck(){
    $communicatieObject.functionCalls.hdCSVCheck = 0
    $csvPath = readFromUI -controlName iCSVPath -propertyName Text
    pushToUILog -logLine "Checking if this is a valid CSV file......."
    try{
        $communicatieObject.hdCSV = @(Import-CSV -Path $csvPath -Delimiter $CSVdelimiter -ErrorAction Stop)
    }catch{
        pushToUILog -logLine "FAILED" -append
        pushToUILog -logLine "$($Error[0])"
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Error importing CSV file, please correct the CSV file and try again", "$scriptName v$scriptVersion" , 0)
        return
    }
    if($communicatieObject.hdCSV[0].path.Length -lt 1){
        pushToUILog -logLine "FAILED" -append
        pushToUILog -logLine "Check 1: invalid CSV file, did you use the right delimiter ($CSVDelimiter) for your system?  Bad data: $($communicatieObject.hdCSV[0])"
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Error importing CSV file, please correct the CSV file and try again", "$scriptName v$scriptVersion" , 0)
        return
    }
    if(-NOT [System.IO.Directory]::Exists($communicatieObject.hdCSV[0].path)){
        pushToUILog -logLine "FAILED" -append
        pushToUILog -logLine "Check 2: could not access $($communicatieObject.hdCSV[0].path)"
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Error importing CSV file, please correct the CSV file and try again", "$scriptName v$scriptVersion" , 0)
        return
    }
    if($communicatieObject.hdCSV[0].username.Length -lt 5 -OR $communicatieObject.hdCSV[0].username.IndexOf(".") -eq -1 -OR $communicatieObject.hdCSV[0].username.IndexOf("@") -eq -1){
        pushToUILog -logLine "FAILED" -append
        pushToUILog -logLine "Check 3: invalid username $($communicatieObject.hdCSV[0].username), specify as XXXX@DOMAIN.SUFFIX"
        $OUTPUT= [System.Windows.Forms.MessageBox]::Show("Error importing CSV file, please correct the CSV file and try again", "$scriptName v$scriptVersion" , 0)
        return
    }
    $communicatieObject.status.hdCSVimported = 1
    pushToUILog -logLine "SUCCEEDED" -append
    pushToUILog -logLine "Imported $($communicatieObject.hdCSV.Count) users from $csvPath"
    pushToUILog -logLine "You may now make the data compliant first (Verify) or verify and upload right away (Upload)"
}

function hdVerifyNoCommit(){
    $communicatieObject.functionCalls.hdVerifyNoCommit = 0
    $communicatieObject.status.Uploading_ByteCount = 0
    $communicatieObject.status.verifying_totalSize = 0
    pushToUILog -logLine "Enqueing $($communicatieObject.hdCSV.Count) homedirectories to verify...."
    $indexIncrement = $jobIndex
    foreach($entry in $communicatieObject.hdCSV){
        $communicatieObject.jobList+= "cleanerNoCommit.ps1" 
        $params = @{"communicatieObject"= $communicatieObject; "path" = $entry.path; "username" = $entry.username; "adminLog" = $verifyLogPath; "commit"=0; "mode"=0; "folderIgnoreList"=$folderIgnoreList}  
        $communicatieObject.jobParameters.$indexIncrement = $params
        $indexIncrement++
        $script:waitingJobs++
        $script:totalJobs++
    }
    pushToUILog -logLine "DONE" -append
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    pushToUILog -logline "Checking all folders from the CSV for potential issues and correcting them automatically..."
    $communicatieObject.status.hdVerifyingNoCommit = $communicatieObject.hdCSV.Count
    $communicatieObject.status.hdVerifyingNoCommit_done = 0
    $communicatieObject.status.hdVerifyingNoCommit_failed = 0
}

function folderVerifyNoCommit(){
    $communicatieObject.functionCalls.folderVerifyNoCommit = 0
    $communicatieObject.status.folderVerifyingItemCount = 0
    $communicatieObject.status.Uploading_ByteCount = 0
    $communicatieObject.status.verifying_totalSize = 0
    $communicatieObject.status.folderVerifiedNoCommit = 0
    $path = readFromUI -controlName iFolderPath -propertyName Text
    $library = readFromUI -controlName iLibName -propertyName Text
    $spURL = readFromUI -controlName iSpURL -propertyName Text
    pushToUILog -logLine "Starting verification of $path...."
    $indexIncrement = $jobIndex
    $communicatieObject.jobList+= "cleaner_spNoCommit.ps1" 
    $params = @{"communicatieObject"= $communicatieObject; "path" = $path; "username" = $library; "adminLog" = $verifyLogPath; "commit"=0; "mode"=0; "folderIgnoreList"=$folderIgnoreList}  
    $communicatieObject.jobParameters.$indexIncrement = $params
    $script:waitingJobs++
    $script:totalJobs++
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    $communicatieObject.status.folderVerifyingNoCommit = 1
    $communicatieObject.status.folderVerifyingNoCommit_done = 0
    $communicatieObject.status.folderVerifyingNoCommit_failed = 0
}

function monitor_hdVerifyNoCommit(){
    $finishedVerifyJobs = $communicatieObject.status.hdVerifyingNoCommit_done
    $totalVerifyJobs = $communicatieObject.status.hdVerifyingNoCommit
    try{$percentComplete = ($finishedVerifyJobs / $totalVerifyJobs) * 100}catch{$percentComplete = 0}    $size = $communicatieObject.status.verifying_totalSize
    $todoSizeString = returnSizeString -bytes $size    setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanning source folders....$todoSizeString"    setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete
    if($finishedVerifyJobs -eq $totalVerifyJobs){
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanned source folders, total size: $todoSizeString"
        setUI -controlName "iBar" -propertyName "value" -newValue 100
        pushToUILog -logline "DONE" -append
        $communicatieObject.status.hdVerifyingNoCommit = 0
        if($communicatieObject.status.hdVerifyingNoCommit_failed -gt 0){
            pushToUILog -logLine "Failures reported: $($communicatieObject.status.hdVerifyingNoCommit_failed), see $($verifyLogPath) for details"
            pushToUILog -logLine "It is recommended to first correct the errors and then run the scan again before uploading."
        }else{
            pushToUILog -logLine "Homedirectories scanned: $($communicatieObject.status.hdVerifyingNoCommit_done), please check $($verifyLogPath) for details on what we will correct in Scan and Fix mode"
        }
    }
}

function monitor_folderVerifyNoCommit(){
    $finishedVerifyJobs = $communicatieObject.status.folderVerifyingNoCommit_done
    $totalVerifyJobs = $communicatieObject.status.folderVerifyingNoCommit
    try{$percentComplete = ($finishedVerifyJobs / $totalVerifyJobs) * 100}catch{$percentComplete = 0}    setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete
    $size = $communicatieObject.status.verifying_totalSize
    $todoSizeString = returnSizeString -bytes $size
    setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanning source folder....$todoSizeString"
    if($finishedVerifyJobs -eq $totalVerifyJobs){
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanned source folder, total size: $todoSizeString"
        setUI -controlName "iBar" -propertyName "value" -newValue 100
        pushToUILog -logline "DONE" -append
        $communicatieObject.status.folderVerifyingNoCommit = 0
        $communicatieObject.status.folderVerifiedNoCommit = 1
        if($communicatieObject.status.folderVerifyingNoCommit_failed -gt 0){
            pushToUILog -logLine "Errors reported, see $($verifyLogPath) for details"
            pushToUILog -logLine "It is recommended to first correct the errors and then run the scan again before uploading."
        }else{
            pushToUILog -logLine "Folder scanned, view the log for anything that would be fixed in Scan and Fix mode: $($verifyLogPath)"
        }
        if($communicatieObject.status.folderVerifyingItemCount -ge 5000){
            pushToUILog -logLine "WARNING: source folder contains more than 5000 items. Threshold limits may apply, see https://en.share-gate.com/blog/demystifying-the-sharepoint-list-thresholds"
        }
    }
}

function hdVerify(){
    $communicatieObject.functionCalls.hdVerify = 0
    $communicatieObject.status.Uploading_ByteCount = 0
    $communicatieObject.status.verifying_totalSize = 0
    pushToUILog -logLine "Enqueing $($communicatieObject.hdCSV.Count) homedirectories to verify...."
    $indexIncrement = $jobIndex
    foreach($entry in $communicatieObject.hdCSV){
        $communicatieObject.jobList+= "cleaner.ps1" 
        $params = @{"communicatieObject"= $communicatieObject; "path" = $entry.path; "username" = $entry.username; "adminLog" = $verifyLogPath; "commit"=1; "mode"=0; "folderIgnoreList"=$folderIgnoreList}  
        $communicatieObject.jobParameters.$indexIncrement = $params
        $indexIncrement++
        $script:waitingJobs++
        $script:totalJobs++
    }
    pushToUILog -logLine "DONE" -append
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    pushToUILog -logline "Checking all folders from the CSV for potential issues and correcting them automatically..."
    $communicatieObject.status.hdVerifying = $communicatieObject.hdCSV.Count
    $communicatieObject.status.hdVerifying_done = 0
    $communicatieObject.status.hdVerifying_failed = 0
}

function folderVerify(){
    $communicatieObject.functionCalls.folderVerify = 0
    $communicatieObject.status.folderVerifyingItemCount = 0
    $communicatieObject.status.Uploading_ByteCount = 0
    $communicatieObject.status.verifying_totalSize = 0
    $communicatieObject.status.folderVerified = 0
    $path = readFromUI -controlName iFolderPath -propertyName Text
    $library = readFromUI -controlName iLibName -propertyName Text
    $spURL = readFromUI -controlName iSpURL -propertyName Text
    pushToUILog -logLine "Starting verification of $path...."
    $indexIncrement = $jobIndex
    $communicatieObject.jobList+= "cleaner_sp.ps1" 
    $params = @{"communicatieObject"= $communicatieObject; "path" = $path; "username" = $library; "adminLog" = $verifyLogPath; "commit"=1; "mode"=0; "folderIgnoreList"=$folderIgnoreList}  
    $communicatieObject.jobParameters.$indexIncrement = $params
    $script:waitingJobs++
    $script:totalJobs++
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    $communicatieObject.status.folderVerifying = 1
    $communicatieObject.status.folderVerifying_done = 0
    $communicatieObject.status.folderVerifying_failed = 0
}

function monitor_hdVerify(){
    $finishedVerifyJobs = $communicatieObject.status.hdVerifying_done
    $totalVerifyJobs = $communicatieObject.status.hdVerifying
    try{$percentComplete = ($finishedVerifyJobs / $totalVerifyJobs) * 100}catch{$percentComplete = 0}    $size = $communicatieObject.status.verifying_totalSize
    $todoSizeString = returnSizeString -bytes $size    setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanning source folders....$todoSizeString"    setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete
    if($finishedVerifyJobs -eq $totalVerifyJobs){
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanned source folders, total size: $todoSizeString"
        setUI -controlName "iBar" -propertyName "value" -newValue 100
        pushToUILog -logline "DONE" -append
        $communicatieObject.status.hdVerifying = 0
        if($communicatieObject.status.hdVerifying_failed -gt 0){
            pushToUILog -logLine "Failures reported: $($communicatieObject.status.hdVerifying_failed), see $($verifyLogPath) for details"
            pushToUILog -logLine "It is recommended to first correct the errors and then run the scan again before uploading."
        }else{
            pushToUILog -logLine "Homedirectories scanned: $($communicatieObject.status.hdVerifying_done), no errors, you may now upload your data."
        }
    }
}

function monitor_folderVerify(){
    $finishedVerifyJobs = $communicatieObject.status.folderVerifying_done
    $totalVerifyJobs = $communicatieObject.status.folderVerifying
    try{$percentComplete = ($finishedVerifyJobs / $totalVerifyJobs) * 100}catch{$percentComplete = 0}    setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete
    $size = $communicatieObject.status.verifying_totalSize
    $todoSizeString = returnSizeString -bytes $size
    setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanning source folder....$todoSizeString"
    if($finishedVerifyJobs -eq $totalVerifyJobs){
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanned source folder, total size: $todoSizeString"
        setUI -controlName "iBar" -propertyName "value" -newValue 100
        pushToUILog -logline "DONE" -append
        $communicatieObject.status.folderVerifying = 0
        $communicatieObject.status.folderVerified = 1
        if($communicatieObject.status.folderVerifying_failed -gt 0){
            pushToUILog -logLine "Errors reported, see $($verifyLogPath) for details"
            pushToUILog -logLine "It is recommended to first correct the errors and then run the scan again before uploading."
        }else{
            pushToUILog -logLine "Folder scanned and corrected, no errors, you may now upload your data."
        }
        if($communicatieObject.status.folderVerifyingItemCount -ge 5000){
            pushToUILog -logLine "WARNING: source folder contains more than 5000 items. Threshold limits may apply, see https://en.share-gate.com/blog/demystifying-the-sharepoint-list-thresholds"
        }
    }
}

function hdUpload(){
    $communicatieObject.functionCalls.hdUpload = 0
    $communicatieObject.status.Uploading_ByteCount = 0
    $communicatieObject.status.verifying_totalSize = 0
    $communicatieObject.status.hdUploading_Starttime = Get-Date
    setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Please wait...."
    $baseURL = "https://$($communicatieObject.status.o365tenant)-my.sharepoint.com/personal/"
    pushToUILog -logLine "Enqueing $($communicatieObject.hdCSV.Count) homedirectories to upload...."
    $indexIncrement = $jobIndex
    $login = readFromUI -controlName iLoginBox -propertyName Text
    $password = readFromUI -controlName iPasswordBox -propertyName Password
    $subFolder = readFromUI -controlName iHDSubFolder -propertyName Text
    foreach($entry in $communicatieObject.hdCSV){
        $communicatieObject.jobList+= "uploader.ps1" 
        $targetURL = $entry.username.Replace(".","_").Replace("@","_")
        $targetURL = "$($baseURL)$($targetURL)"
        $params = @{"communicatieObject"= $communicatieObject; "o365username" = $login; "o365password" = $password; "sourceFolder" = $entry.path; "log" = $verifyLogPath; "targetURL"=$targetURL; "baseScriptPath" = $scriptPath; "cleanMode" = 0; "targetFolder" = $subFolder; "folderIgnoreList"=$folderIgnoreList; "username" = $entry.username}  
        $communicatieObject.jobParameters.$indexIncrement = $params
        $indexIncrement++
        $script:waitingJobs++
        $script:totalJobs++
    }
    pushToUILog -logLine "DONE" -append
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    pushToUILog -logline "Uploading all folders from the CSV..."
    $communicatieObject.status.hdUploading = $communicatieObject.hdCSV.Count
    $communicatieObject.status.hdUploading_done = 0
    $communicatieObject.status.hdUploading_failed = 0
    $communicatieObject.status.Uploading_failedObjectCount = 0
    $communicatieObject.status.Uploading_failedObjects = @()
}

function folderUpload(){
    $communicatieObject.functionCalls.folderUpload = 0
    $communicatieObject.status.folderUploading_done = 0
    $communicatieObject.status.folderUploading = 0
    $communicatieObject.status.Uploading_ByteCount = 0
    $communicatieObject.status.folderUploading_Starttime = Get-Date
    setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Please wait...."
    $path = readFromUI -controlName iFolderPath -propertyName Text
    $library = readFromUI -controlName iLibName -propertyName Text
    $spURL = readFromUI -controlName iSpURL -propertyName Text  
    $subFolder = readFromUI -controlName iSpSubFolder -propertyName Text
    $login = readFromUI -controlName iLoginBox -propertyName Text
    $password = readFromUI -controlName iPasswordBox -propertyName Password
    $indexIncrement = $jobIndex
    try{
        $rootObjects = Get-ChildItem -LiteralPath $path -ErrorAction Stop
    }catch{
        pushToUILog -logLine "Failed to read from $path $($Error[0])"
        return -1
    }

    pushToUILog -logLine "Queueing folders as seperate jobs"
    #First queue the root folder files seperately
    $communicatieObject.status.folderUploading++
    $communicatieObject.jobList+= "uploader_sp.ps1" 
    $params = @{"communicatieObject"= $communicatieObject; "o365username" = $login; "o365password" = $password; "sourceFolder" = $path; "log" = $verifyLogPath; "targetURL"=$spURL; "baseScriptPath" = $scriptPath; "cleanMode" = 3; "libraryName" = $library; "specificFolder" = "CMD_EXCEPTION_FILES_ONLY"; "targetFolder" = $subFolder; "folderIgnoreList"=$folderIgnoreList}  
    $communicatieObject.jobParameters.$indexIncrement = $params
    $indexIncrement++
    $script:waitingJobs++
    $script:totalJobs++  
    #Now queue each folder
    foreach($object in $rootObjects){
        if($object.PSIsContainer){
            $communicatieObject.status.folderUploading++
            $communicatieObject.jobList+= "uploader_sp.ps1" 
            $params = @{"communicatieObject"= $communicatieObject; "o365username" = $login; "o365password" = $password; "sourceFolder" = $path; "log" = $verifyLogPath; "targetURL"=$spURL; "baseScriptPath" = $scriptPath; "cleanMode" = 3; "libraryName" = $library; "specificFolder" = $object.Name; "targetFolder" = $subFolder; "folderIgnoreList"=$folderIgnoreList}  
            $communicatieObject.jobParameters.$indexIncrement = $params
            $indexIncrement++
            $script:waitingJobs++
            $script:totalJobs++      
        }
    }

    pushToUILog -logLine "Folders queued for upload, uploading will now commence"
    setUI -controlName "iBar" -propertyName "value" -newValue 0
    $communicatieObject.status.folderUploading_failed = 0
    $communicatieObject.status.Uploading_failedObjectCount = 0
    $communicatieObject.status.Uploading_failedObjects = @()
}

function monitor_hdUpload(){
    $uploadLog = $env:APPDATA+"\O365MigratorDataUploaderResults.log"
    $finishedUploadJobs = $communicatieObject.status.hdUploading_done
    $totalUploadJobs = $communicatieObject.status.hdUploading
    $bytes = $communicatieObject.status.Uploading_ByteCount
    $size = $communicatieObject.status.verifying_totalSize
    $doneSizeString = returnSizeString -bytes $bytes
    $todoSizeString = returnSizeString -bytes $size
    $starttime = $communicatieObject.status.hdUploading_Starttime

    try{$percentComplete = ($bytes / $size) * 100}catch{$percentComplete = 0}
    if($percentComplete -gt 0){        
        $runtime = ((Get-Date) - $starttime).TotalSeconds
        $timeleft = ((100/$percentComplete)*$runtime)-$runtime
        $ts =  [timespan]::fromseconds($timeleft)
        $remainingTime = "$("{0:hh\:mm\:ss}" -f $ts) time"
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "$($doneSizeString[0])$($doneSizeString[1]) of $($todoSizeString[0])$($todoSizeString[1]) of uploaded, $remainingTime left, $($communicatieObject.status.Uploading_failedObjectCount) errors"
    }else{
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanning source folders....$todoSizeString"
    }    setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete
    if($finishedUploadJobs -eq $totalUploadJobs){
        setUI -controlName "iBar" -propertyName "value" -newValue 100
        pushToUILog -logline "DONE" -append
        $communicatieObject.status.hdUploading = 0
        $ts =  [timespan]::fromseconds($runtime)
        $tookTime = "$("{0:hh\:mm\:ss}" -f $ts)"
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "$($doneSizeString[0])$($doneSizeString[1]) done, took $tookTime, $($communicatieObject.status.Uploading_failedObjectCount) errors"
        pushToUILog -logLine "$($doneSizeString[0])$($doneSizeString[1]) done, took $tookTime"
        if($communicatieObject.status.hdUploading_failed -gt 0 -or $communicatieObject.status.Uploading_failedObjectCount -gt 0){
            pushToUILog -logLine "Failed homedirectories reported: $($communicatieObject.status.hdUploading_failed) see $($uploadLog) for details"
            pushToUILog -logLine "$($communicatieObject.status.Uploading_failedObjectCount) failed items reported: see $($uploadLog) for details"
            pushToUILog -logLine "It is recommended to correct the errors and then run the upload again, any existing files in O4B will be overwritten."
        }else{
            pushToUILog -logLine "Homedirectories uploaded: $($communicatieObject.status.hdUploading_done)"
        }
    }
}

function monitor_folderUpload(){
    $uploadLog = $env:APPDATA+"\O365MigratorDataUploaderResults.log"
    $finishedUploadJobs = $communicatieObject.status.folderUploading_done
    $totalUploadJobs = $communicatieObject.status.folderUploading
    $bytes = $communicatieObject.status.Uploading_ByteCount
    $size = $communicatieObject.status.verifying_totalSize
    $doneSizeString = returnSizeString -bytes $bytes
    $todoSizeString = returnSizeString -bytes $size
    $starttime = $communicatieObject.status.folderUploading_Starttime

    try{$percentComplete = ($bytes / $size) * 100}catch{$percentComplete = 0}
    if($percentComplete -gt 0){        
        $runtime = ((Get-Date) - $starttime).TotalSeconds
        $timeleft = ((100/$percentComplete)*$runtime)-$runtime
        $ts =  [timespan]::fromseconds($timeleft)
        $remainingTime = "$("{0:hh\:mm\:ss}" -f $ts) time"
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "$($doneSizeString[0])$($doneSizeString[1]) of $($todoSizeString[0])$($todoSizeString[1]) of uploaded, $remainingTime left, $($communicatieObject.status.Uploading_failedObjectCount) errors"
    }else{
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "Scanning source folder....$todoSizeString"
    }
    setUI -controlName "iBar" -propertyName "value" -newValue $percentComplete
    if($finishedUploadJobs -eq $totalUploadJobs){
        setUI -controlName "iBar" -propertyName "value" -newValue 100
        $communicatieObject.status.folderUploading = 0
        $ts =  [timespan]::fromseconds($runtime)
        $tookTime = "$("{0:hh\:mm\:ss}" -f $ts)"
        setUI -controlName "iUploadedStatus" -propertyName "Content" -newValue "$($doneSizeString[0])$($doneSizeString[1]) done, took $tookTime, $($communicatieObject.status.Uploading_failedObjectCount) errors"
        pushToUILog -logLine "$($doneSizeString[0])$($doneSizeString[1]) done, took $tookTime"
        if($communicatieObject.status.folderUploading_failed -gt 0 -or $communicatieObject.status.Uploading_failedObjectCount -gt 0){
            if($communicatieObject.status.folderUploading_failed -gt 0){
                pushToUILog -logLine "Critical issues encountered in completing job, see $($uploadLog) for details"
            }
            if($communicatieObject.status.Uploading_failedObjectCount -gt 0){
                pushToUILog -logLine "$($communicatieObject.status.Uploading_failedObjectCount) failed items reported, see $($uploadLog) for details"
            }
            pushToUILog -logLine "It is recommended to correct the errors and then run the upload again or upload them manually, any existing files in the library will be overwritten."
        }else{
            pushToUILog -logLine "Finished uploading."
        }
    }
}

while($finishedJobs -lt $totalJobs){
    #start a job if there are threads and waiting jobs
    if($RunspacePool.GetAvailableRunspaces() -gt 0 -and $waitingJobs -gt 0){
        try{
            $Jobs += startThread -communicatieObject $communicatieObject -scriptName $communicatieObject.jobList[$jobIndex] -parameters ($communicatieObject.jobParameters.$jobIndex)
            $jobIndex++
            $runningJobs++
            $waitingJobs--
        }catch{
            $jobIndex++
            $finishedJobs++
            $waitingJobs--
            pushToUILog -logLine "Failure in starting job! Restart O365Migrator. $($Error[0])"
        }
    }
    #dispose of finished Jobs
    ForEach ($Job in $($Jobs | Where-Object {$_.Handle.IsCompleted -eq $True})){
        $finishedJobs++
        $runningsJobs--
        try{
            $result = $Job.Thread.EndInvoke($Job.Handle)
        }catch{
            $result = $False
        }
        #callback to specific batches of jobs
        if($Job.Object -Match "cleanerNoCommit.ps1"){
            $communicatieObject.status.hdVerifyingNoCommit_done++
            if(-NOT $result -eq $True){
                $communicatieObject.status.hdVerifyingNoCommit_failed++
            }
        }
        if($Job.Object -Match "cleaner_spNoCommit.ps1"){
            $communicatieObject.status.folderVerifyingNoCommit_done++
            if(-NOT $result -eq $True){
                $communicatieObject.status.folderVerifyingNoCommit_failed++
            }
        }
        if($Job.Object -Match "cleaner.ps1"){
            $communicatieObject.status.hdVerifying_done++
            if(-NOT $result -eq $True){
                $communicatieObject.status.hdVerifying_failed++
            }
        }
        if($Job.Object -Match "cleaner_sp.ps1"){
            $communicatieObject.status.folderVerifying_done++
            if(-NOT $result -eq $True){
                $communicatieObject.status.folderVerifying_failed++
            }
        }
        if($Job.Object -Match "uploader_sp.ps1"){
            $communicatieObject.status.folderUploading_done++
            if(-NOT $result -eq $True){
                $communicatieObject.status.folderUploading_failed++
            }
        }
        if($Job.Object -Match "uploader.ps1"){
            $communicatieObject.status.hdUploading_done++
            if(-NOT $result -eq $True){
                $communicatieObject.status.hdUploading_failed++
            }
        }

        if($result -eq $True){
            Write-Host "$($Job.Object) thread finished" -ForegroundColor Green
        }else{
            Write-Host "$($Job.Object) failed to start or finish properly: $result" -ForegroundColor Red
        }
        $Job.Thread.Dispose()
        $Job.Thread = $Null
        $Job.Handle = $Null
    }
    try{$percentComplete = ($finishedJobs / ($totalJobs)) * 100}catch{$percentComplete = 0}

    #listen for function calls / dispatches from within jobs and run them:
    $functionsToRun = @()
    $communicatieObject.functionCalls.Keys | % {
        if($communicatieObject.functionCalls.$_ -eq 1){
            $functionsToRun += [String]$_
        }
    }
    $functionsToRun | % {Invoke-Expression $_}

    #monitor callback to UI for long running functions
    if($communicatieObject.status.hdVerifyingNoCommit -gt 0){
        monitor_hdVerifyNoCommit
    }
    if($communicatieObject.status.folderVerifyingNoCommit -gt 0){
        monitor_folderVerifyNoCommit
    }
    if($communicatieObject.status.hdVerifying -gt 0){
        monitor_hdVerify
    }
    if($communicatieObject.status.folderVerifying -gt 0){
        monitor_folderVerify
    }
    if($communicatieObject.status.hdUploading -gt 0){
        monitor_hdUpload
    }
    if($communicatieObject.status.folderUploading -gt 0){
        monitor_folderUpload
    }

    #monitor / report status
    $RunningJobTitles = "$($($Jobs | Where-Object {$_.Handle.IsCompleted -eq $False}).Object)"
    Write-Progress -Activity "$($maxThreads - $($RunspacePool.GetAvailableRunspaces())) of $maxThreads available threads active" -PercentComplete $percentComplete -Status "$finishedJobs of $($totalJobs) job threads completed." 
    Start-Sleep -Milliseconds $SleepTimer  
}

$RunspacePool.Close() | Out-Null
$RunspacePool.Dispose() | Out-Null

Write-Host "Finished, good bye!" -ForegroundColor Green

Pause
